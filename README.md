# Pet Store (Cucumber - Serenity)

## Introduction

This is an Automation Framework to test the available endpoints on https://petstore.swagger.io/v2/swagger.json API.
The main stack of technologies is Cucumber, Serenity, RestAssured & Maven

## Project Structure

```
src
 + test
   + java
     + common
       CommonActions                API Calls used between all models
       CommonQuestions              Common validations on the response 
     + pets
     + stepsdefinitions             Step Definitions for  Models
     + store
     + user
     CucumberTestSuite
   + Resources
     +features                      Features Directory
     +schema                        JSON Schemas Directory
     serenity.conf                  Configuration File
```
The project has 12 scenarios distributed between the three features files:

- src/test/resources/pets.feature
- src/test/resources/store.feature
- src/test/resources/user.feature

### Pets
- Scenario: Users are able to perform a CRUD to pets in the system
- Scenario: Users are able to create a pet in the system
- Scenario: Users are able to update a pet in the system 
- Scenario: Users are able to update a pet in the system
- Scenario: Pet response should match the expected schema

### Store
- Scenario: Users are able to perform a CRUD to orders in the system
- Scenario: Users are able to create an order in the system
- Scenario: Users are able to delete a pet in the system
- Scenario: Users are able to crate and order with an unavailable pet in the system

### User
- Scenario: Users are able to perform a CRUD to users in the system
- Scenario: Deleting a not existing user should return 404
- Scenario: Getting a not existing user should return 404


## Installing the Framework 
You will need to run the following command in order to get the Framework
```
git clone https://gitlab.com/juanmanuelag95/petstore.git
```

## Executing the Framework
Run `mvn clean verify` from the command line from root directory.

```
You will need:
- Java 1.8 installed (I tried JDK 14 and fail with serenity) 
- Maven Installed 
```

The test results will be recorded here `target/site/serenity/index.html`.

Reports can be seen in Gitlab CI Pipeline doing the following steps:
1. Go to the latest pipeline
2. Click on test stage and open
3. In the right Menu, click on *Browse*
4. Search for `target/site/serenity/index.html`.

## Adding Test
You can add new scenarios to one of the existing features the only condition is that you need to add the tag **@Test** in order to run with the others  scenarios.


