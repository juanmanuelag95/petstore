Feature: Order Tests
  This feature includes tests that test the store order RESTFul services

  @E2E @Store
  Scenario: Users are able to perform a CRUD to orders in the system
    Given the Swagger Petstore API is available
    When I place an order for a pet with an order id '6'
    Then the request response has a '200' response code
    And the order requests response contains the correct json data
    Then I am able to get the order
    Then the request response has a '200' response code
    And the order requests response contains the correct json data
    Then I can delete an order
    Then the request response has a '200' response code
    And check if the order is actually deleted
    Then the request response has a '404' response code

  @Test @S_01 @Store
  Scenario: Users are able to create a order in the system
    Given the Swagger Petstore API is available
    When I place an order for a pet with an order id '6'
    Then the request response has a '200' response code
    And the order requests response contains the correct json data

  @Test @S_02 @Store
  Scenario: Users are able to delete a pet in the system
    Given the Swagger Petstore API is available
    When I place an order for a pet with an order id '6'
    Then the request response has a '200' response code
    Then I can delete an order
    Then the request response has a '200' response code
    And check if the order is actually deleted
    Then the request response has a '404' response code

# Current behavior of the API - ignores if the pet is unavailable
  @Test @S_03 @Store
  Scenario: Users are able to crate and order with an unavailable pet in the system
    Given the Swagger Petstore API is available
    When a pet have a status of "unavailable"
    When I place an order for a pet with an order id '0'
    Then the request response has a '200' response code
