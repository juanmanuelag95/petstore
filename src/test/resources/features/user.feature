Feature: User Tests
  This feature includes tests that test the user RESTFul services

  @E2E @User
  Scenario: Users are able to perform a CRUD to users in the system
    Given the Swagger Petstore API is available
    When I add a User to the system
    Then the request response has a '200' response code
    Then I can update tha name of the user "Juan"
    Then the request response has a '200' response code
    And validate that the value "Juan" in user is updated
    Then user is able to login
    Then the request response has a '200' response code
    And  I am able to delete the user
    Then the request response has a '200' response code
    And check if the user is actually deleted
    Then the request response has a '404' response code

  @Test @U_01 @User
  Scenario: Deleting a not existing user should return 404
    Given the Swagger Petstore API is available
    And I am able to delete the user with id "3312"
    Then the request response has a '404' response code

  @Test @U_02 @User
  Scenario: Getting a not existing user should return 404
    Given the Swagger Petstore API is available
    And I am able to get the user with id "3312"
    Then the request response has a '404' response code
    And the user response error should have the following values