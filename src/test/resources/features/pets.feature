Feature: Pet Tests
  This feature includes tests that test the pet RESTFul services

  @E2E @Pet
  Scenario: Users are able to perform a CRUD to pets in the system
    Given the Swagger Petstore API is available
    When I add a Pet to the system
    Then the request response has a '200' response code
    Then I can update tha name of the pet "Ipsum"
    Then the request response has a '200' response code
    And validate that the value "Ipsum" in pet is updated
    Then I can delete a pet
    Then the request response has a '200' response code
    And check if the pet is actually deleted
    Then the request response has a '404' response code

    @Test @P_01 @Pet
    Scenario: Users are able to create a pet in the system
      Given the Swagger Petstore API is available
      When I add a Pet to the system with data table
      | CategoryId | 2             |
      | Category   | Canine        |
      | Name       | Lorem         |
      | PhotosUrl  | https://image |
      | TagId      | 2             |
      | Tag        | nice          |
      | Status     | available     |

      Then the request response has a '200' response code

    @Test @P_02 @Pet
    Scenario: Users are able to update a pet in the system
      Given the Swagger Petstore API is available
      When I add a Pet to the system
      Then the request response has a '200' response code
      Then I can update tha name of the pet "Ipsum"
      Then the request response has a '200' response code
      And validate that the value "Ipsum" in pet is updated

    @Test @P_03 @Pet
    Scenario: Users are able to delete a pet in the system
      Given the Swagger Petstore API is available
      When I add a Pet to the system
      Then the request response has a '200' response code
      Then I can delete a pet
      Then the request response has a '200' response code
      And check if the pet is actually deleted
      Then the request response has a '404' response code

    @Test @P_04 @Pet
    Scenario: Pet response should match the expected schema
      Given the Swagger Petstore API is available
      When I add a Pet to the system
      Then the request response has a '200' response code
      And the respond body should match with the expected schema "pet.json"
