package stepdefinitions;

import common.CommonQuestions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import org.json.JSONObject;
import store.StoreActions;
import java.io.IOException;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class StoreSteps {

    @Steps
    StoreActions storeActions;

    @Steps
    CommonQuestions commonQuestions;

    @When("^I place an order for a pet with an order id '(\\d+)'$")
    public void i_can_place_an_order_with_id(int id) throws IOException {

//        storeActions.setProperty("orderId", Integer.toString(id));
        Serenity.setSessionVariable("orderId").to(id);
        long petId;

//        Check if an petId already exits
//        if exist: Set petId to session variable
//        if not: Set petId to default
        if (Serenity.sessionVariableCalled("petId") != null){
            petId = Serenity.sessionVariableCalled("petId");
        } else {
            petId = 54321;
        }

        String json = new JSONObject()
                .put("id", id)
                .put("petId", petId)
                .put("quantity", 1)
                .put("shipDate", "2020-12-29T03:39:48.768Z")
                .put("status", "placed")
                .put("complete", true)
                .toString();

        storeActions.addStore(json);
    }

    @And("^the order requests response contains the correct json data$")
    public void the_order_request_response_has_the_correct_response_body() {
        int id = Serenity.sessionVariableCalled("orderId");

        String expectedBody = new JSONObject()
                .put("id", id)
                .put("petId", 54321)
                .put("quantity", 1)
                .put("shipDate", "2020-12-29T03:39:48.768+0000")
                .put("status", "placed")
                .put("complete", true)
                .toString();

        commonQuestions.responseBodyIs(expectedBody, lastResponse().asString());
    }

    @Then("^I am able to get the order$")
    public void i_am_able_to_get_the_order() {
        storeActions.getOrder(Serenity.sessionVariableCalled("orderId"));
    }

    @Then("^I can delete an order$")
    public void i_delete_a_order_in_the_system() {
        storeActions.deleteOrder(Serenity.sessionVariableCalled("orderId"));
    }

    @And("^check if the order is actually deleted$")
    public void verify_the_delete_a_order_in_the_system() {
        storeActions.getOrder(Serenity.sessionVariableCalled("orderId"));
    }
}
