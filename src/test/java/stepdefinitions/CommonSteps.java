package stepdefinitions;

import common.CommonActions;
import common.CommonQuestions;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class CommonSteps {
    @Steps
    CommonActions commonActions;

    @Steps
    CommonQuestions commonQuestions;

    @Given("^the Swagger Petstore API is available$")
    public void the_swagger_petstore_api_is_available() {
        commonActions.validAPI();
        commonQuestions.responseCodeIs(200, lastResponse());
    }

    @Then("^the request response has a '(\\d+)' response code$")
    public void the_pet_request_response_has_the_correct_response_code(Integer expectedStatus) {
        commonQuestions.responseCodeIs(expectedStatus, lastResponse());
    }
}
