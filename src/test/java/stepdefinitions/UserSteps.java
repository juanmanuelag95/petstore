package stepdefinitions;

import common.CommonQuestions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.path.json.JsonPath;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import org.json.JSONObject;
import user.UserActions;
import user.UserQuestions;

import java.io.IOException;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class UserSteps {

    @Steps
    UserActions userActions;

    @Steps
    UserQuestions userQuestions;

    @Steps
    CommonQuestions commonQuestions;

    @When("^I add a User to the system$")
    public void i_add_a_user_to_the_system() throws IOException {

        long userId = userActions.setId();

        Serenity.setSessionVariable("userId").to(userId);
        Serenity.setSessionVariable("username").to("Cancun" + Long.toString(Serenity.sessionVariableCalled("userId")));

        String json = new JSONObject()
                                .put("id", userId)
                                .put("username", "Cancun" + Long.toString(userId))
                                .put("firstName", "Lorem")
                                .put("lastName", "Ipsum")
                                .put("email", "test@test.com")
                                .put("password", "1234")
                                .put("phone", "9876543201")
                                .put("userStatus", 1)
                        .toString();

        userActions.addUser(json);
    }

    @When("^I can update tha name of the user \"(.*)\"$")
    public void i_update_name_of_user_in_the_system(String name) {

        long userId = Serenity.sessionVariableCalled("userId");

        String json =  new JSONObject()
                        .put("id", userId)
                        .put("username", "Cancun" + Long.toString(userId))
                        .put("firstName", name)
                        .put("lastName", "Ipsum")
                        .put("email", "test@test.com")
                        .put("password", "1234")
                        .put("phone", "9876543201")
                        .put("userStatus", 1)
                        .toString();

        userActions.updateUser(Serenity.sessionVariableCalled("username"), json);
    }

    @And("^validate that the value \"(.*)\" in user is updated$")
    public void i_validate_user_value_in_the_system(String expectedName) {
        userActions.getUser(Serenity.sessionVariableCalled("username"));

        JsonPath jsonPathEvaluator = lastResponse().jsonPath();
        String updatedName = jsonPathEvaluator.get("firstName");

        userQuestions.checkUpdateValue(expectedName, updatedName);
    }

    @Then("^user is able to login$")
    public void user_is_able_to_login_into_system() {
        userActions.loginUser(Serenity.sessionVariableCalled("username"), "1234");
    }

    @And("^I am able to delete the user$")
    public void able_to_delete_user_in_the_system() {
        userActions.deleteUser(Serenity.sessionVariableCalled("username"));
    }

    @And("^check if the user is actually deleted$")
    public void verify_the_delete_a_user_in_the_system() {
        userActions.getUser(Serenity.sessionVariableCalled("username"));
    }

    @And("^I am able to delete the user with id \"(.*)\"$")
    public void verify_the_delete_by_id_a_user_in_the_system(String id) {
        userActions.deleteUser(id);
    }

    @And("^I am able to get the user with id \"(.*)\"$")
    public void verify_get_by_id_a_user_in_the_system(String id) {
        userActions.getUser(id);
    }

    @And("^the user response error should have the following values$")
    public void the_order_request_response_has_the_correct_response_body() {

        String expectedBody = new JSONObject()
                .put("code", 1)
                .put("type", "error")
                .put("message", "User not found")
                .toString();

        commonQuestions.responseBodyIs(expectedBody, lastResponse().asString());
    }
}