package stepdefinitions;

import common.CommonQuestions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.path.json.JsonPath;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import org.json.JSONArray;
import pets.PetActions;
import org.json.JSONObject;
import pets.PetQuestions;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class PetsSteps {

    @Steps
    PetActions petActions;

    @Steps
    PetQuestions petQuestions;

    @Steps
    CommonQuestions commonQuestions;

    @When("^I add a Pet to the system$")
    public void i_add_a__pet_to_the_system() throws IOException {

        long petId = petActions.setId();

        Serenity.setSessionVariable("petId").to(petId);

        String json = new JSONObject()
                .put("id", petId)
                .put("category", new JSONObject()
                                .put("id", 2)
                                .put("name", "Canine"))
                .put("name", "Lorem")
                .put("photoUrls", new JSONArray().put("http://pathtoimage1"))
                .put("tags", new JSONArray().put(
                        new JSONObject()
                            .put("id", 2)
                            .put("name", "nice")))
                .put("status", "available")
                .toString();

        petActions.addPet(json);
    }

    @When("^I add a Pet to the system with data table$")
    public void i_add_a__pet_to_the_system(Map<String, String> values) {
        HashMap<String, String> petData = new HashMap<String, String>(values);

        long petId = petActions.setId();

        Serenity.setSessionVariable("petId").to(petId);

        String json = new JSONObject()
                .put("id", petId)
                .put("category", new JSONObject()
                        .put("id", Integer.parseInt(petData.get("CategoryId")))
                        .put("name", petData.get("Category")))
                .put("name", petData.get("Name"))
                .put("photoUrls", new JSONArray().put(petData.get("PhotosUrl")))
                .put("tags", new JSONArray().put(
                        new JSONObject()
                                .put("id", Integer.parseInt(petData.get("TagId")))
                                .put("name", petData.get("Tag"))))
                .put("status", petData.get("Status"))
                .toString();

        petActions.addPet(json);
    }

    @When("^I can update tha name of the pet \"(.*)\"$")
    public void i_update_pet_in_the_system(String name) {
       petActions.updatePet(Serenity.sessionVariableCalled("petId"), name);
    }

    @And("^validate that the value \"(.*)\" in pet is updated$")
    public void i_validate_pet_value_in_the_system(String expectedName) {
        petActions.getPet(Serenity.sessionVariableCalled("petId"));

        JsonPath jsonPathEvaluator = lastResponse().jsonPath();
        String updatedName = jsonPathEvaluator.get("name");

        petQuestions.checkUpdateValue(expectedName, updatedName);
    }

    @Then("^I can delete a pet$")
    public void i_delete_a_pet_in_the_system() {
        petActions.deletePet(Serenity.sessionVariableCalled("petId"));
    }

    @And("^check if the pet is actually deleted$")
    public void verify_the_delete_a_pet_in_the_system() {
        petActions.getPet(Serenity.sessionVariableCalled("petId"));
    }

    @And("the respond body should match with the expected schema \"(.*)\"")
    public void the_schema_should_match_with_the_specification(String spec) {
        commonQuestions.verifyResponseSchema(lastResponse(), spec);
    }

    @When("^a pet have a status of \"(.*)\"$")
    public void creating_a_pet_with_an_specific_status(String petStatus) {
        long petId = petActions.setId();

        Serenity.setSessionVariable("petId").to(petId);

        String json = new JSONObject()
                .put("id", petId)
                .put("category", new JSONObject()
                        .put("id", 2)
                        .put("name", "Canine"))
                .put("name", "Mexico")
                .put("photoUrls", new JSONArray().put("http://pathtoimage1"))
                .put("tags", new JSONArray().put(
                        new JSONObject()
                                .put("id", 2)
                                .put("name", "nice")))
                .put("status", petStatus)
                .toString();

        petActions.addPet(json);
    }
}