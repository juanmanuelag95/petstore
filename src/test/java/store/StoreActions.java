package store;

import common.CommonRequestSpec;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.Properties;

public class StoreActions {

//    private final String PROPERTIES_PATH = "src/test/java/store/store.properties";
    private final String BASE_PATH = "store/order";
//    private Properties properties;

//    public StoreActions() throws FileNotFoundException, IOException {
//        this.properties = new Properties();
//        this.properties.load(new FileInputStream(PROPERTIES_PATH));
//    }
//    public void setProperty(String property, String value) throws IOException {
//        FileOutputStream out = new FileOutputStream(PROPERTIES_PATH);
//
//        this.properties.setProperty(property, value);
//        this.properties.store(out, null);
//        out.close();
//    }

    public Response addStore(String json) {
        return SerenityRest.given().spec(CommonRequestSpec.blogReqSpec("application/json"))
                .basePath(BASE_PATH)
                .body(json)
                .post().then().extract().response();
    }

    public Response getOrder(int id) {
        return SerenityRest.given().spec(CommonRequestSpec.blogReqSpec("x-www-form-urlencoded"))
                .basePath(BASE_PATH)
                .get("/{id}", id).then().extract().response();
    }

    public Response deleteOrder(int id) {
        return SerenityRest.given().spec(CommonRequestSpec.blogReqSpec("x-www-form-urlencoded"))
                .basePath(BASE_PATH)
                .delete("/{id}", id).then().extract().response();
    }
}
