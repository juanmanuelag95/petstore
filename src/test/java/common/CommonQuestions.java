package common;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import io.restassured.response.Response;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.junit.Assert.assertEquals;

public class CommonQuestions {
    public void responseCodeIs(int responseCode, Response lastResponse) {
        assertEquals(lastResponse.statusCode(), responseCode);
    }

    public void verifyResponseSchema(Response lastResponse, String schemaJson) {
        lastResponse.then().body(matchesJsonSchemaInClasspath("schema/" + schemaJson));
    }

    public void responseBodyIs(String expectedBody, String actualBody) {
        JsonParser parser = new JsonParser();

        JsonElement expectedJson = parser.parse(expectedBody);
        JsonElement actualJson = parser.parse(actualBody);

        assertEquals(expectedJson, actualJson);
    }
}
