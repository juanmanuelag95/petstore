package common;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

public class CommonActions {
    public Response validAPI() {
        return SerenityRest.given().spec(CommonRequestSpec.blogReqSpec("application/json"))
                .basePath("swagger.json")
                .get().then().extract().response();
    }
}
