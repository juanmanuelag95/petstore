package common;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

public class CommonRequestSpec {
    public static RequestSpecification blogReqSpec(String contentType) {
        EnvironmentVariables environmentVariables = Injectors.getInjector()
                .getInstance(EnvironmentVariables.class);

        String baseUrl = EnvironmentSpecificConfiguration.from(environmentVariables)
                .getProperty("baseurl");

        if (contentType.equals("application/json")){
            return new RequestSpecBuilder().setBaseUri(baseUrl)
                .setContentType(contentType)
                .build();
        }else{
            return new RequestSpecBuilder().setBaseUri(baseUrl)
                    .setContentType(ContentType.URLENC.withCharset("UTF-8"))
                    .build();
        }

    }
}
