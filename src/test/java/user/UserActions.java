package user;

import common.CommonRequestSpec;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import java.util.Calendar;
import java.util.Date;

public class UserActions {

    private final String BASE_PATH = "user";

    public long setId() {
        Date dateOne = Calendar.getInstance().getTime();
        return dateOne.getTime();
    }

    public Response addUser(String json) {
        return SerenityRest.given().spec(CommonRequestSpec.blogReqSpec("application/json"))
                .basePath(BASE_PATH)
                .body(json)
                .post().then().extract().response();
    }

    public Response updateUser(String id, String json) {
        return SerenityRest.given().spec(CommonRequestSpec.blogReqSpec("application/json"))
                .basePath(BASE_PATH)
                .body(json)
                .put("/{id}", id).then().extract().response();
    }

    public Response getUser(String id) {
        return SerenityRest.given().spec(CommonRequestSpec.blogReqSpec("x-www-form-urlencoded"))
                .basePath(BASE_PATH)
                .get("/{id}", id).then().extract().response();
    }

    public Response loginUser(String username, String password) {
        return SerenityRest.given().spec(CommonRequestSpec.blogReqSpec("x-www-form-urlencoded"))
                .basePath(BASE_PATH)
                .param("username", username)
                .param("password", password)
                .get("/login").then().extract().response();
    }

    public Response deleteUser(String id) {
        return SerenityRest.given().spec(CommonRequestSpec.blogReqSpec("x-www-form-urlencoded"))
                .basePath(BASE_PATH)
                .delete("/{id}", id).then().extract().response();
    }
}
