package pets;

import org.junit.Assert;

public class PetQuestions {

    public void checkUpdateValue(String expectedName, String updatedName) {
        Assert.assertEquals(updatedName, expectedName);
    }
}
