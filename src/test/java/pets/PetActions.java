package pets;

import common.CommonRequestSpec;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import java.util.Calendar;
import java.util.Date;

public class PetActions {

    private final String BASE_PATH = "pet";

    public long setId() {
        Date dateOne = Calendar.getInstance().getTime();
        return dateOne.getTime();
    }

    public Response addPet(String json) {
        return SerenityRest.given().spec(CommonRequestSpec.blogReqSpec("application/json"))
                .basePath(BASE_PATH)
                .body(json)
                .post().then().extract().response();
    }

    public Response updatePet(long id, String name) {
        return SerenityRest.given().spec(CommonRequestSpec.blogReqSpec("x-www-form-urlencoded"))
                .basePath(BASE_PATH)
                .formParam("name", name)
                .post("/{id}", id).then().extract().response();
    }

    public Response getPet(long id) {
        return SerenityRest.given().spec(CommonRequestSpec.blogReqSpec("x-www-form-urlencoded"))
                .basePath(BASE_PATH)
                .get("/{id}", id).then().extract().response();
    }

    public Response deletePet(long id) {
        return SerenityRest.given().spec(CommonRequestSpec.blogReqSpec("x-www-form-urlencoded"))
                .basePath(BASE_PATH)
                .delete("/{id}", id).then().extract().response();
    }
}
